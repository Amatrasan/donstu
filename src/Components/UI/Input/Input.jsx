import React, { useState } from 'react'
import { Eye, EyeClose } from '../Icons/Icons'
import './input.scss'

/*
className = className
classNameInput
titleText = text
title=boolan
showError = boolean
errorText = text
placeholder = placeholder
classNameBox = for box (input + icon)
colorMessage = blue, red
*/

const Input = (props) => {
  const [visibility, setVisibility] = useState(false)
  return (
    <div className={`input_ui_main ${props.className || ''}`}>
      {props.title ? <div className="input_ui_title">{props.titleText}</div> : null}

      {!props.icon ? (
        props.refBool ? (
          <div><input
            ref={props.refInput || null}
            type={props.type || 'text'}
            onClick={props.onClick || null}
            onBlur={props.onBlur || null}
            onFocus={props.onFocus || null}
            disabled={props.disabled || null}
            defaultValue={props.value}
            id={props.id || null}
            name={props.name || null}
            placeholder={props.placeholder || null}
            className={`input_ui_input 222 ${props.classNameInput || ''}`}/>
          </div>
        ) : (<div><input
          type={props.type || 'text'}
          onClick={props.onClick || null}
          onBlur={props.onBlur || null}
          onFocus={props.onFocus || null}
          disabled={props.disabled || null}
          value={props.value || ''}
          onChange={props.onChange || null}
          id={props.id || null}
          name={props.name || null}
          placeholder={props.placeholder || null}
          className={`input_ui_input 111 ${props.classNameInput || ''}`}/>
        </div>)
      ) : (
        <div className={`input_ui_box ${props.classNameBox || ''}`}>
          <input
            type={visibility ? 'text' : 'password'}
            onClick={props.onClick}
            onBlur={props.onBlur}
            disabled={props.disabled}
            value={props.value || ''}
            onChange={props.onChange}
            onKeyPress={props.onKeyPress}
            id={props.id || null}
            autoComplete={'new-password'}
            name={props.name || null}
            placeholder={props.placeholder || null}
            className={`input_ui_input input_ui_input_icon ${props.classNameInput || ''}`}/>
          <div className="input_ui_icon"
            onTouchStart={(e) => { e.preventDefault(); setVisibility(!visibility) }}
            onTouchEnd={(e) => { e.preventDefault(); setVisibility(!visibility) }}
            onMouseDown={() => setVisibility(!visibility)}
            onMouseUp={() => setVisibility(!visibility)}
          >
            {!visibility ? <Eye /> : <EyeClose />}
          </div>
        </div>
      )
      }
      {props.showError ? <div className={`input_ui_message input_ui_message_${props.colorMessage || 'red'}`}>{props.errorText}</div> : null}
    </div>

  )
}

export default Input
