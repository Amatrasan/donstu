import React from 'react'
import PropTypes from 'prop-types'

import { Redirect, Route } from 'react-router-dom'

const RoutePrivate = ({component: Component, isAuthenticated, ...rest}) => (
  <Route
    {...rest}
    render={props => (isAuthenticated ? <Component {...props} /> : (
      <Redirect
        to={{
          pathname: '/login',
          state: {from: props.location}
        }}
      />
    ))
    }
  />
)

RoutePrivate.propTypes = {
  component: PropTypes.oneOfType([PropTypes.func, PropTypes.shape({})]).isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
  location: PropTypes.shape({})
}

RoutePrivate.defaultProps = {
  location: undefined
}

export default RoutePrivate
