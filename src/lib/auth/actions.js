import api from '../../services/ApiServices'
import history from '../../services/HistoryService'

import * as types from './actionTypes'

export function updateTokens (tokens) {
  return {
    type: types.LOG_IN,
    payload: tokens
  }
}

export function deleteTokens () {
  return {
    type: types.LOG_OUT
  }
}

export function authLogin (credentials, callback = null) {
  return dispatch => api.authLogin(credentials).then(() => {
    // dispatch(getUser())
    console.log('then')
    callback && callback()
  }).catch(e => {
    console.log('catch')
    debugger
    history.push('/members')
    console.log('error', e)
  })
}

export function authLogout () {
  return () => api.authLogout()
}

