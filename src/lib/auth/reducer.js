import * as types from './actionTypes'

const initialState = {
  tokens: undefined
}

const auth = (state = initialState, {type, payload}) => {
  switch (type) {
    case types.LOG_IN:
      return {
        ...state,
        tokens: payload
      }

    case types.LOG_OUT:
      return initialState

    default:
      return state
  }
}

export default auth
