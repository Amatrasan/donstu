import React from 'react'
import './App.scss'
import { Redirect, Route, Router, Switch } from 'react-router-dom'
import Login from '../Login'
import Members from '../../Components/Members'

import history from '../../services/HistoryService'

const App = (props) => {
  // const isLogged = useSelector(state => state)

  return (
    <div className="App">
      <Router history={history}>
        <Switch>
          <Route path="/" component={Login}/>
          <Route path="/members" component={Members}/>
          {/* /<RoutePrivate path="/members" component={Members} isAuthenticated={true} /> */}
          {/* {!isLogged && <Redirect to="/login"/>} */}
          <Redirect to="/"/>
        </Switch>
      </Router>
    </div>
  )
}

export default App
