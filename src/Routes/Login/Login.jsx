import React from 'react'
import './Login.scss'
import { useFormik } from 'formik'
import * as Yup from 'yup'
import Input from '../../Components/UI/Input'
import {useDispatch} from 'react-redux'
import { authLogin } from '../../lib/auth/actions'
import { useHistory } from 'react-router'

const Login = () => {
  const dispatch = useDispatch()
  const history = useHistory()
  console.log(history)
  const formik = useFormik({
    initialValues: {
      login: '',
      password: ''
    },
    validationSchema: Yup.object({
      login: Yup.string()
        .required('Введите Логин'),
      password: Yup.number()
        .required('Введите пароль')
    }),
    onSubmit: values => {
      history.push('/members')
      // 12
      // dispatch(authLogin({email: values.login, password: values.password}))
    }
  })
  return (
    <div className="login">
      <div className="login-form">
        <form onSubmit={formik.handleSubmit}>
          <div className="login-form-title">Вход</div>
          <Input
            title={true}
            titleText="Логин"
            name="login"
            showError={true}
            placeholder="Логин"
            errorText={formik.errors.login}
            onChange={formik.handleChange}
            onBlur={formik.onBlur}
            value={formik.values.login}
          />
          <Input
            name="password"
            title={true}
            titleText="Пароль"
            showError={true}
            icon={true}
            placeholder="Пароль"
            errorText={formik.errors.password}
            onChange={formik.handleChange}
            onBlur={formik.onBlur}
            value={formik.values.password}
          />
          <button type="submit">Войти</button>
        </form>

      </div>
    </div>
  )
}

export default Login
