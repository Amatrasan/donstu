import React from 'react'
import ReactDOM from 'react-dom'
import App from './Routes/App/App'
import {Provider} from 'react-redux'
import * as serviceWorker from './serviceWorker'
import reduxStore from './services/StoreService'

const store = reduxStore.get()
const persistor = reduxStore.getPersistor()

const render = (Component) => {
  ReactDOM.render(
    <Provider store={store}>
      <Component/>
    </Provider>,
    document.getElementById('root')
  )
}

persistor.then(() => render(App))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
