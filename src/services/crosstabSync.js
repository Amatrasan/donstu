// based on https://github.com/rt2zz/redux-persist-crosstab

export function crosstabSync (store, config, storagePrefix, actionType) {
  const blacklist = config.blacklist || null
  const whitelist = config.whitelist || null
  const storeKey = `${storagePrefix}${config.key}`

  function handleStorageEvent (e) {
    if (e.key === storeKey) {
      if (e.oldValue === e.newValue) {
        return
      }

      const statePartial = JSON.parse(e.newValue)
      const state = Object.keys(statePartial).reduce((newState, reducerKey) => {
        if (whitelist && whitelist.indexOf(reducerKey) === -1) {
          return newState
        }

        if (blacklist && blacklist.indexOf(reducerKey) !== -1) {
          return newState
        }

        return {
          ...newState,
          [reducerKey]: JSON.parse(statePartial[reducerKey])
        }
      }, {})

      store.dispatch({
        key: config.key,
        payload: state,
        type: actionType
      })
    }
  }

  window.addEventListener('storage', handleStorageEvent, false)
}
