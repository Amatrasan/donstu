import {applyMiddleware, compose, createStore} from 'redux'
import thunk from 'redux-thunk'
import {KEY_PREFIX, persistReducer, persistStore, REHYDRATE} from 'redux-persist'
import storage from 'redux-persist/lib/storage'

import {crosstabSync} from './crosstabSync'

import rootReducer from '../lib'

const persistConfig = {
  key: 'donstu',
  storage,
  whitelist: [
    'auth',
    'user',
    'navigation'
  ],
  version: 1
}

class StoreService {
  constructor () {
    this.store = this.createStore()
    this.persistor = new Promise(resolve => persistStore(this.store, null, () => resolve()))

    crosstabSync(this.store, persistConfig, KEY_PREFIX, REHYDRATE)
  }

  createStore () {
    /* eslint-disable no-underscore-dangle */
    const persistedReducer = persistReducer(persistConfig, rootReducer)
    const __REDUX_DEVTOOLS_EXTENSION__ = window.__REDUX_DEVTOOLS_EXTENSION__
    const initialState = {}
    const middlewares = [thunk]

    return createStore(
      persistedReducer,
      initialState,
      compose(
        applyMiddleware(...middlewares),
        (__REDUX_DEVTOOLS_EXTENSION__ && process.env.REACT_APP_APP_ENV !== 'production')
          ? __REDUX_DEVTOOLS_EXTENSION__() : f => f
      )
    )
    /* eslint-enable */
  }

  get () {
    return this.store
  }

  getPersistor () {
    return this.persistor
  }
}

const store = new StoreService()

export default store
