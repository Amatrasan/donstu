import axios from 'axios'

class Api {
  authLogin ({ email, password }) {
    return axios.post('/', {email: email, password: password})
  }
}

export default new Api()
